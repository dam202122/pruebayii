<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <div class="jumbotron esquinasredondas">
    <h1><?= Html::encode($this->title) ?></h1>
    <h2>Pues mira, hay un h2</h2>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>
    </div>
</div>
